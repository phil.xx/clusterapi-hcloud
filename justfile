init:
	#!/usr/bin/env sh
	source .envrc

	kind create cluster --name capi-hcloud || true

	kind get kubeconfig --name capi-hcloud > ./kindconfig

	export EXP_CLUSTER_RESOURCE_SET=true

	kubectl --kubeconfig=./kindconfig create secret generic hetzner-token --from-literal=token=$HCLOUD_TOKEN

	clusterctl init --infrastructure hcloud:v0.1.4

config:
	#!/usr/bin/env sh
	source .envrc

	helm template ./chart/ \
	--set cluster.name=$CLUSTERNAME \
	-f ./chart/values.yaml \
	-f chart/defaults/values.yaml \
	> cluster.yaml

kindconfig:
	#!/usr/bin/env sh
	source .envrc

	kind get kubeconfig --name capi-hcloud > ./kindconfig

kubeconfig:
	#!/usr/bin/env sh
	source .envrc

	kubectl --kubeconfig=./kindconfig -n default get secret $CLUSTERNAME-kubeconfig \
	-o jsonpath={.data.value} | base64 --decode \
	> ./kubeconfig

cluster-create: init kindconfig config
    source .envrc

	kubectl --kubeconfig=./kindconfig apply -f cluster.yaml

#cluster-prepare: kubeconfig cilium rook metallb
#cluster-prepare: kubeconfig cilium
