# cluster-api-hcloud

[Documentation](https://github.com/cluster-api-provider-hcloud/cluster-api-provider-hcloud)

## Requirements

- clusterctl
- just
- kind
- kubectl
- Helm

## Prerequisites

We need to register the __hcloud__ infrastructure provider in `$HOME/.cluster-api/clusterctl.yaml`:

```yaml
providers:
  - name: "hcloud"
    url: "https://github.com/cluster-api-provider-hcloud/cluster-api-provider-hcloud/releases/latest/infrastructure-components.yaml"
    type: "InfrastructureProvider"
```
## Workflow
- `just init` Initializes kind management cluster and clusterctl
- `just config` Genereates `cluster.yaml`from the provided Helm-Chart
- `just cluster-create` Creates the CRD in local kind cluster and starts deploying the cluster to hcloud (includes init, kindconfig & config)
- `just kindconfig` Get kind kubeconfig saved to `./kindconfig`
- `just kubeconfig` Get hcloud kubeconfig saved to `./kubeconfig`

